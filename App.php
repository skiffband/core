<?php

require __DIR__.'/Core.php';

use \skiff\Core;
use \skiff\components\request\interfaces\Request;
use \skiff\components\router\interfaces\Router;


/**
 * @property string $name Application name
 * @property \skiff\components\request\Request $request request object
 * @property \skiff\components\router\Router $router router object
 */
class App extends Core
{
    /**
     * Application instance
     *
     * @var $_app App instance
     */
    private static $_app;

    private $_name = 'Skiff Framework';

    private $_request;

    private $_router;

    private function __construct() {}

    private function __clone() {}

    /**
     * @return App instance
     */
    public static function get() : self
    {
        if(is_null(self::$_app))
            self::$_app = new self();

        return self::$_app;
    }

    /**
     * Building app
     * @param array $params
     */
    public function init(array $params)
    {
        $this->build($params)->start();
    }


    /**
     *
     */
    public function start()
    {
        $this->router->request = $this->request;
        $this->router->route();
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->_name = $name;
    }

    /**
     * @return Request component object
     *
     */
    public function getRequest(): Request
    {
        return $this->_request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * @return Router
     */
    public function getRouter() : Router
    {
        return $this->_router;
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->_router = $router;
    }
}
