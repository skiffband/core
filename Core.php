<?php

declare(strict_types=1);

namespace skiff;

use skiff\base\Object;

define('APP_CORE_DIR', __DIR__);

define('APP_BASE_DIR', dirname(APP_CORE_DIR, 3));

define('APP_TEMP_DIR', APP_BASE_DIR.'/temp');

define('APP_ROOT_URI', '/');

require APP_CORE_DIR.'/base/Object.php';

require APP_CORE_DIR.'/Autoloader.php';

class Core extends Object
{

}
Autoloader::$namespace = require_once APP_CORE_DIR.'/namespace.php';
Autoloader::init();


