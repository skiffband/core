<?php

namespace skiff\base;

use skiff\helpers\StringHelper;

class Object
{
    public function __get(string $name)
    {
        $name = StringHelper::snakeToCamel($name);
        return $this->{'get'.$name}();
    }

    public function __set(string $name, $value)
    {
        $name = StringHelper::snakeToCamel($name);
        $this->{'set'.$name}($value);
    }

    /**
     * Object builder
     * @param array $params
     * @return self $this
     */
    protected function build(array $params) : self
    {
        foreach ($params as $attribute => $value)
        {
            if(isset($value['class']))
                $value = new $value['class'];

            $this->$attribute = $value;
        }

        return $this;
    }
}