<?php


namespace skiff\base\view;

use skiff\base\controller\interfaces\Controller;
use skiff\base\view\interfaces\View as ViewInterface;

class View implements ViewInterface
{
    private $_view_dir = APP_BASE_DIR.'/views/';

    private $_layout = APP_BASE_DIR.'/views/layouts/main.php';

    private $_name;

    private $_params;

    private $_controller;

    public function __construct(string $name, array $params = [], Controller $controller)
    {
        $this->_name = $name;
        $this->_params = $params;
        $this->_controller = $controller;
    }

    public function __toString()
    {
        ob_start();
        $viewDir = $this->getControllerName($this->_controller);
        extract($this->_params);
        require $this->_view_dir . $viewDir . '/' . $this->_name . '.php';
        $content = ob_get_clean();
        return require $this->_layout;

    }

    private function getControllerName(Controller $controller)
    {
        $controllerName = explode('\\',get_class($controller));
        $controller = $controllerName[count($controllerName) - 1];
        return strtolower(str_replace('Controller', '', $controller));
    }

    public function setName(string $name)
    {
        $this->_name = $name;
    }

    public function getName() : string
    {
        return $this->_name;
    }

    public function setParams(array $params)
    {
        $this->_params = $params;
    }

    public function getParams() : array
    {
        return $this->_params;
    }

    public function setLayout(string $layout)
    {
        $this->_layout = $layout;
    }

    public function getLayout() : string
    {
        return $this->_layout;
    }

    public function setController(Controller $controller)
    {
        $this->_controller = $controller;
    }

    public function getController() : Controller
    {
        return $this->_controller;
    }

}