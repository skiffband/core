<?php

namespace skiff\base\controller;

use \skiff\base\view\View;
use \skiff\base\controller\interfaces\Controller as ControllerInterface;


/**
 * Class Controller
 *
 *
 */
class Controller implements ControllerInterface
{
    public function view(string $name, array $params = []) : View
    {
        return new View($name, $params, $this);
    }
}