<?php
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 21.09.17
 * Time: 9:39
 */
return [
	'skiff\base\controller\Controller'            => APP_CORE_DIR . '/base/controller/Controller.php',
	'skiff\base\controller\interfaces\Controller' => APP_CORE_DIR . '/base/controller/interfaces/Controller.php',
	'skiff\base\view\View'                        => APP_CORE_DIR . '/base/view/View.php',
	'skiff\base\view\interfaces\View'             => APP_CORE_DIR . '/base/view/interfaces/View.php',
	
	'skiff\components\router\Route'               => APP_CORE_DIR . '/components/router/Route.php',
	'skiff\components\router\interfaces\Route'    => APP_CORE_DIR . '/components/router/interfaces/Route.php',
	'skiff\components\router\Router'              => APP_CORE_DIR . '/components/router/Router.php',
	'skiff\components\router\interfaces\Router'   => APP_CORE_DIR . '/components/router/interfaces/Router.php',
	'skiff\components\request\Request'            => APP_CORE_DIR . '/components/request/Request.php',
	'skiff\components\request\interfaces\Request' => APP_CORE_DIR . '/components/request/interfaces/Request.php',
	'skiff\components\request\Url'                => APP_CORE_DIR . '/components/request/Url.php',
	'skiff\components\request\interfaces\Url'     => APP_CORE_DIR . '/components/request/interfaces/Url.php',
	
	'skiff\helpers\StringHelper'                  => APP_CORE_DIR . '/helpers/StringHelper.php'
];