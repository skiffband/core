<?php

namespace skiff;

class Autoloader
{
	public static $namespace = [];
	
    public static function init()
    {
        spl_autoload_register([self::class, 'autoload'], true, true);
        self::loadRouters();
    }

    public static function autoload(string $className)
    {
    	if(isset(self::$namespace[$className]))
    		$class = self::$namespace[$className];
    	else
    		$class = APP_BASE_DIR.'/'.str_replace('\\', '/', $className).'.php';
        
        include_once $class;
    }

    public static function loadRouters()
    {
        require APP_BASE_DIR.'/config/router.php';
        require APP_CORE_DIR.'/components/router/config/config.php';
    }
}



