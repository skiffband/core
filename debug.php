<?php

define('APP_LOG_DIR', APP_TEMP_DIR.'/log');

error_reporting(E_ALL);

ini_set('display_startup_errors', (string)DEBUG_MODE);

ini_set('display_errors', "1");

ini_set('log_errors', APP_LOG_DIR.'/log.txt');