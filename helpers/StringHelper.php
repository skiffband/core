<?php

namespace skiff\helpers;


class StringHelper
{
    /**
     * Formatting snake case string to camel case string
     * @param string $snake_string snake string
     * @param bool $skipFirstWord  skip first word
     *
     * @return string camel case string
     */
    public static function snakeToCamel(string $snake_string, bool $skipFirstWord = false) : string
    {
        if(strpos($snake_string, '_') === false) return ucfirst($snake_string);

        $parts = explode('_', $snake_string);
        $camel_string = '';
        for($i = 0; $i <= count($parts) - 1; $i++)
        {
            if($skipFirstWord)
            {
                $camel_string .= $parts[$i];
                $skipFirstWord = false;
                continue;
            }

            $camel_string .= ucfirst($parts[$i]);
        }

        return $camel_string;
    }
}