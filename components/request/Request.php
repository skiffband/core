<?php

namespace skiff\components\request;

use skiff\base\Object;
use skiff\components\request\interfaces\Request as RequestInterface;


/**
 * Class Request
 *
 * @property string $url
 * @property string $query_string return http method GET, POST,...
 * @property array $uri return request headers
 * @property array $host return request host
 *
 */
class Request extends Object implements RequestInterface
{

    private $_url;

    public function __construct()
    {
        $this->url = new Url();
    }

    /**
     * @return array
     */
    public function get() : array
    {
        return $_GET;
    }

    public function post() : array
    {
        return $_POST;
    }

    /**
     * @return Url
     */
    public function getUrl() : Url
    {
        return $this->_url;
    }

    /**
     * @param Url $url
     */
    public function setUrl(Url $url)
    {
        $this->_url = $url;
    }

}