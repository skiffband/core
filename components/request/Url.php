<?php

namespace skiff\components\request;

use skiff\base\Object;
use \skiff\components\request\interfaces\Url as UrlInterface;

/**
 * Class Url
 *
 * @property string $scheme
 * @property string $host
 * @property string $port
 * @property string $user
 * @property string $pass
 * @property string $path
 * @property string $query
 * @property string $fragment
 */
class Url extends Object implements UrlInterface
{
    private $_scheme;

    private $_host;

    private $_port;

    private $_user;

    private $_pass;

    private $_path;

    private $_query;

    private $_fragment;

    public function __construct()
    {
        $url = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $this->build(parse_url($url));
    }

    /**
     * @return string
     */
    public function getScheme() : string
    {
        return $this->_scheme;
    }

    /**
     * @param string $scheme
     */
    public function setScheme(string $scheme)
    {
        $this->_scheme = $scheme;
    }

    /**
     * @return string
     */
    public function getHost() : string
    {
        return $this->_host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->_host = $host;
    }

    /**
     * @return string
     */
    public function getPort() : string
    {
        return $this->_port;
    }

    /**
     * @param string $port
     */
    public function setPort(string $port)
    {
        $this->_port = $port;
    }

    /**
     * @return string
     */
    public function getUser() : string
    {
        return $this->_user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user)
    {
        $this->_user = $user;
    }

    /**
     * @return string
     */
    public function getPass() : string
    {
        return $this->_pass;
    }

    /**
     * @param string $pass
     */
    public function setPass(string $pass)
    {
        $this->_pass = $pass;
    }

    /**
     * @return string
     */
    public function getPath() : string
    {
        return $this->_path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->_path = $path;
    }

    /**
     * @return string
     */
    public function getQuery() : string
    {
        return $this->_query;
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query)
    {
        $this->_query = $query;
    }

    /**
     * @return string
     */
    public function getFragment() : string
    {
        return $this->_fragment;
    }

    /**
     * @param string $fragment
     */
    public function setFragment(string $fragment)
    {
        $this->_fragment = $fragment;
    }


}