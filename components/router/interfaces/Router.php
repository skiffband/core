<?php

namespace skiff\components\router\interfaces;


interface Router
{
    public function route();
}