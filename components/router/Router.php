<?php

namespace skiff\components\router;

use skiff\base\Object;
use skiff\components\request\interfaces\Request;
use \skiff\components\router\interfaces\Router as RouterInterface;
use \skiff\components\router\interfaces\Route;
use \Exception;

/**
 * Class Router
 *
 * @property \skiff\components\request\Request $request
 * @property Route $route
 */
class Router extends Object implements RouterInterface
{
    private $_request;

    private $_route;

    /**
     * @var Route[]
     */
    public static $routers = [];

    public function route()
    {
        $request_url = $this->request->url;
        if(!isset(self::$routers[$request_url->path]))
          throw new Exception('404 Page not found');

        $this->route = self::$routers[$request_url->path];

        (new $this->route->controller)->{$this->route->action}($this->request->get());
    }

    /**
     * @return Request
     */
    public function getRequest() : Request
    {
        return $this->_request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * @return \skiff\components\router\interfaces\Route
     */
    public function getRoute() : Route
    {
        return $this->_route;
    }

    /**
     * @param \skiff\components\router\interfaces\Route $route
     */
    public function setRoute(Route $route)
    {
        $this->_route = $route;
    }
}