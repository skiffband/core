<?php

namespace skiff\components\router;

use skiff\base\Object;
use skiff\components\router\interfaces\Route as RouteInterface;


/**
 * Class Route
 *
 * @property string $urn
 * @property string $controller
 * @property string $action
 */
class Route extends Object implements RouteInterface
{
    private $_urn;

    private $_controller;

    private $_action;

    /**
     * @param string $urn
     * @param array $params
     */
    public static function set(string $urn, array $params)
    {
        Router::$routers[$urn] = new Route($params);
    }


    public function __construct(array $params = [])
    {
        $data = explode('::', current($params));
        $this->controller = $data[0];
        $this->action = $data[1];
    }

    /**
     * @return string
     */
    public function getUrn() : string
    {
        return $this->_urn;
    }

    /**
     * @param mixed $urn
     */
    public function setUrn(string $urn)
    {
        $this->_urn = $urn;
    }

    /**
     * @return string
     */
    public function getController() : string
    {
        return $this->_controller;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->_controller = $controller;
    }

    /**
     * @return string
     */
    public function getAction() : string
    {
        return $this->_action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->_action = $action;
    }


}